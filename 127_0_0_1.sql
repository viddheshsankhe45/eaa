-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 17, 2019 at 06:38 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eaa`
--
CREATE DATABASE IF NOT EXISTS `eaa` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `eaa`;

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `row_names` text,
  `first_name` text,
  `last_name` text,
  `phone` text,
  `email` text,
  `city` text,
  `skills` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`row_names`, `first_name`, `last_name`, `phone`, `email`, `city`, `skills`) VALUES
('1', 'Kacie', 'Beahan', '049.392.2594', 'Brant.Aufderhar60@hotmail.com', 'South Vallie', '[\"6\",\"17\",\"13\"]'),
('2', 'Jack', 'Sipes', '716.375.4318 x15712', 'Catalina.Greenholt92@yahoo.com', 'East Toy', '[\"24\"]'),
('3', 'Wilson', 'Windler', '1-054-735-4399 x33603', 'Paxton_Metz@gmail.com', 'Lake Catharine', '[\"5\",\"16\",\"8\",\"6\"]'),
('4', 'Ocie', 'Zemlak', '772.560.0966', 'Gay.Kuhic@hotmail.com', 'Kihnport', '[\"20\",\"29\",\"15\",\"21\",\"8\"]'),
('5', 'Hassie', 'Brakus', '1-469-961-2504 x0284', 'Krystina40@yahoo.com', 'Lake Destineeview', '[\"27\",\"1\"]'),
('6', 'Ceasar', 'Brekke', '(268) 215-4584 x854', 'Reymundo.Pagac65@yahoo.com', 'East General', '[\"6\",\"1\",\"4\"]'),
('7', 'Tyrel', 'Schiller', '1-885-045-1960', 'Abe_Abbott@hotmail.com', 'Ankundingfurt', '[\"3\",\"7\",\"24\"]'),
('8', 'Jarred', 'Jones', '379.713.4166', 'Ted68@yahoo.com', 'Gaylordville', '[\"26\",\"16\",\"30\"]'),
('9', 'Sabryna', 'O\'Connell', '(298) 202-4774', 'Marcelle90@gmail.com', 'North Bernie', '[\"12\",\"20\"]'),
('10', 'Pearline', 'Wiza', '1-115-865-2466 x878', 'Wendell.West82@hotmail.com', 'Watersport', '[\"15\"]');

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE `skills` (
  `row_names` text,
  `id` bigint(20) DEFAULT NULL,
  `skills` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`row_names`, `id`, `skills`) VALUES
('1', 1, 'Python'),
('2', 2, 'Java'),
('3', 3, 'R'),
('4', 4, 'Machine Learning'),
('5', 5, 'Data Analysis'),
('6', 6, 'SQLite'),
('7', 7, 'MySQL'),
('8', 8, 'C/C++'),
('9', 9, 'Blockchain'),
('10', 10, 'Firebase'),
('11', 11, 'A/B Testing'),
('12', 12, 'Android'),
('13', 13, 'Apache Kafka'),
('14', 14, 'Big Data'),
('15', 15, 'Algorithms'),
('16', 16, 'Database Management'),
('17', 17, 'Data Mining'),
('18', 18, 'Statistics'),
('19', 19, 'Flask'),
('20', 20, 'Django'),
('21', 21, 'Dialogflow'),
('22', 22, 'Flutter'),
('23', 23, 'React'),
('24', 24, 'React Native'),
('25', 25, 'Javascript'),
('26', 26, 'HTML'),
('27', 27, 'CSS'),
('28', 28, 'AngularJS'),
('29', 29, 'Tableau'),
('30', 30, 'Excel'),
('31', 31, 'Embedded Systems');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
